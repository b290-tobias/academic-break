// console.log("academic break");


// Javascript - Function Parameters and Retrn Statement
function shipItem(item, weight, location) {
	let shippingFee;
	if (weight <= 3) {
		shippingfee = 150;
	} else if (weight <= 5) {
		shippingfee = 280;
	} else if (weight <= 8) {
		shippingfee = 340;
	} else if (weight <= 10) {
		shippingfee = 410;
	} else {
		shippingfee = 560;
	};

	let additionalFee;
	switch(location) {
		case "local": 
			additionalFee = 0;
			break;
		case "international":
			additionalFee = 250;
			break;
		default:
			additionalFee = 0;
	};

	let totalShippingFee = shippingfee + additionalFee;
	return `The total shipping fee for the ${item} that will ship ${location} is ${totalShippingFee}`;
}


console.log(shipItem("bag", 10, "international"));



// JS Objects

function Product(name, price, quantity) {
	this.name = name;
	this.price = price;
	this.quantity = quantity;
};

function Customer(name) {
	this.name = name;
	this.cart = [];
	this.addToCart = function (product) {
		this.cart.push(product);
		console.log(`${product.name} is added to cart`);
	};
	this.removeToCart = function (product){
		this.cart.splice(this.cart.indexOf(product),1);
		console.log(`${product.name} is removed from the cart`);
	};
};


let product1 = new Product("Diapers", 550, 1);
let product2 = new Product("Wet Wipes", 200, 1);
let customer = new Customer("Jules");

console.log(customer.addToCart(product1));
console.log(customer.addToCart(product2));
console.log(customer.cart);
console.log(customer.removeToCart(product2));
console.log(customer.cart);




